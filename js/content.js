require(['c4/paragraphDetection'], function(pd) {
    var paragraphs = pd.getParagraphs(document);
    var mouseTimer;
    var scrollTimer;
    var resizeTimer;
    var focusedParagraph = {};

    // check for window movements every 500ms as there is no dedicated event
    var oldX = window.screenX;
    var oldY = window.screenY;
    var winHandler = function() {
        if (oldX !== window.screenX || oldY !== window.screenY) {
            $(window).trigger('resize');
        }
        oldX = window.screenX;
        oldY = window.screenY;
    };
    var winInterval = setInterval(winHandler, 500);

    /* returns current position/dimension of extracted paragraphs
     * adds a flag to the paragraph detected as focused
     * if the 'full' parameter is set to true, content and headline are added
     */
    var getParagraphs = function(full) {
        var p = [];
        paragraphs.forEach(function(v) {
            var parent = $(v.elements[0]).parent();
            var tmp = {
                id: v.id,
                x: parent.offset().left - $(window).scrollLeft(),
                y: parent.offset().top - $(window).scrollTop(),
                width: parent.width(),
                height: parent.height()
            };
            if (full) {
                tmp.content = v.content;
                tmp.headline = v.headline;
            }
            if (focusedParagraph.id && focusedParagraph.id === tmp.id) {
                tmp.focused = true;
            }
            p.push(tmp);
        });
        return p;
    };

    // returns page/window dimensions and extracted paragraphs
    var pageDetails = function() {
        var logEntry = {
            timestamp:Date.now()
        };
        logEntry.page = window.location.href;
        logEntry.dimensions = {
            screenWidth: screen.width,
            screenHeight: screen.height,
            windowX: window.screenX,
            windowY: window.screenY,
            windowOuterHeight: window.outerHeight,
            windowOuterWidth: window.outerWidth,
            windowInnerHeight: window.innerHeight,
            windowInnerWidth: window.innerWidth,
            jqWindowHeight: $(window).height(),
            jqWindowWidth: $(window).width(),
            jqDocumentHeight: $(document).height(),
            jqDocumentWidth: $(document).width()
        };
        logEntry.paragraphs = getParagraphs(true);
        return logEntry;
    };

    var clickHandler = function(evt) {
        chrome.runtime.sendMessage({method: 'click', data: {
                screenX: evt.screenX,
                screenY: evt.screenY,
                pageX: evt.pageX,
                pageY: evt.pageY,
                timestamp: Date.now(),
                page: window.location.href
            }});
    };

    var scrollHandler = function(evt) {
        clearTimeout(scrollTimer);
        scrollTimer = window.setTimeout(function() {
            var timestamp = Date.now();
            chrome.runtime.sendMessage({method: 'scroll', data: {
                    scrollTop: $(window).scrollTop(),
                    scrollLeft: $(window).scrollLeft(),
                    paragraphs: getParagraphs(),
                    timestamp: timestamp,
                    page: window.location.href
                }});
        }, 100);
    };

    var mouseHandler = function(evt) {
        clearTimeout(mouseTimer);
        mouseTimer = window.setTimeout(function() {
            chrome.runtime.sendMessage({method: 'mouseMoved', data: {
                    pageX: evt.pageX,
                    pageY: evt.pageY,
                    screenX: evt.screenX,
                    screenY: evt.screenY,
                    page: window.location.href,
                    timestamp: Date.now()
                }});
        }, 200);
    };

    var resizeHandler = function(evt) {
        clearTimeout(resizeTimer);
        clearInterval(winInterval);
        resizeTimer = window.setTimeout(function() {
            chrome.runtime.sendMessage({method: 'resize', data: pageDetails()});
        }, 100);
        oldX = window.screenX;
        oldY = window.screenY;
        winInterval = setInterval(winHandler, 500);
    };

    var attachHandlers = function() {
        $(document).click(clickHandler);
        $(document).scroll(scrollHandler);
        $(window).resize(resizeHandler);
        $(document).mousemove(mouseHandler);
        // listen for paragraph focused events and apply, if last focused out of viewport
        $(document).on('paragraphFocused', function(evt) {
            var lastOutOfFocus = false;
            if (typeof focusedParagraph !== 'undefined' && typeof focusedParagraph.elements !== 'undefined') {
                var outTop = $(focusedParagraph.elements[0]).parent().offset().top < $(window).scrollTop();
                var outBottom = $(focusedParagraph.elements[0]).parent().offset().top > $(window).scrollTop() + $(window).height();
                if (outTop || outBottom) {
                    lastOutOfFocus = true;
                }
            } else {
                lastOutOfFocus = true;
            }
            var focusEvent = evt.originalEvent.detail;
            if (focusEvent.paragraph !== null && focusedParagraph !== focusEvent.paragraph && (lastOutOfFocus || focusEvent.trigger === 'click')) {
                // set focused paragraph variable
                focusedParagraph = focusEvent.paragraph;
                var timestamp = Date.now();
                chrome.runtime.sendMessage({method: 'paragraphFocused', data: {paragraphs: getParagraphs(), timestamp: timestamp, page: window.location.href}});
            }
        });
        // start paragraph detection
        pd.findFocusedParagraphSimple();
    };

    chrome.runtime.sendMessage({method: 'isActive'}, function(isActive) {
        if (isActive) {
            attachHandlers();
            chrome.runtime.sendMessage({method: 'newPage', data: pageDetails()});
        }
    });

    chrome.runtime.onMessage.addListener(function(msg, sender, response) {
        if (msg.method && msg.method === 'getDetails') {
            var timestamp = Date.now();
            attachHandlers();
            var tmp = pageDetails();
            tmp.timestamp = timestamp;
            response(tmp);
        }
    });
});
