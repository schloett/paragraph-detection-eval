require(['../js/common'], function(common) {
    require(['jquery'], function($) {
        chrome.tabs.query({'active': true}, function(tabs) {
            var url = tabs[0].url;
            if (url.startsWith('chrome') && !url.startsWith('chrome://newtab')) {
                // do nothing
            } else {
                chrome.runtime.sendMessage({method: 'isActive'}, function(isActive) {
                    if (isActive) {
                        $('#save_log').show();
                    } else {
                        $('#start_log').show();
                    }
                });
            }
        });

        $('#start_log').click(function(e) {
            e.preventDefault();
            chrome.runtime.sendMessage({method: 'startLogging'});
            window.close();
        });
        $('#save_log').click(function(e) {
            e.preventDefault();
            chrome.runtime.sendMessage({method: 'saveLog'}, function(response) {
                console.log(response);
                var result = JSON.stringify(response.log);

                var url = 'data:application/json;base64,' + btoa(unescape(encodeURIComponent(result)));
                chrome.downloads.download({
                    url: url,
                    filename: 'log_' + response.userID + '.json',
                    saveAs:true
                });
                $('#save_log').hide();
            });
//            window.close();
        });
    });
});