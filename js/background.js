require(['./common'], function(common) {
    var logEntry = [];
    var isActive = false;
    // userID just to prefill the log filename
    var userID = window.localStorage.getItem('userID');
    if (!userID) {
        userID = 1;
        window.localStorage.setItem('userID', userID);
    }


    chrome.runtime.onMessage.addListener(function(msg, sender, response) {
        console.log(msg);
        if (msg.method) {
            if (msg.method === 'startLogging') {
                isActive = true;
                logEntry = [];
                // query the currently active tab for window/page details
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {method: 'getDetails'}, function(response) {
                        response.action = 'initial';
                        logEntry.push(response);
                    });
                });
            } else if (msg.method === 'click' && isActive) {
                msg.data.action = 'click';
                logEntry.push(msg.data);
            } else if (msg.method === 'newPage' && isActive) {
                msg.data.action = 'newPage';
                logEntry.push(msg.data);
            } else if (msg.method === 'saveLog') {
                response({log:logEntry,userID:userID});
                isActive = false;
                userID++;
                window.localStorage.setItem('userID', userID);
            } else if (msg.method === 'mouseMoved' && isActive) {
                msg.data.action = 'mouseMoved';
                logEntry.push(msg.data);
            } else if (msg.method === 'resize' && isActive) {
                msg.data.action = 'resize';
                logEntry.push(msg.data);
            } else if (msg.method === 'scroll' && isActive) {
                msg.data.action = 'scroll';
                logEntry.push(msg.data);
            } else if (msg.method === 'paragraphFocused' && isActive) {
                msg.data.action = 'paragraphFocused';
                logEntry.push(msg.data);
            } else if (msg.method === 'isActive') {
                response(isActive);
            }
        }
        ;
    });
});