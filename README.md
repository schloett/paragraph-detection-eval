# Paragraph Detection Evaluation

## ready to install extension
[https://chrome.google.com/webstore/detail/paragraph-detection/klfchfdilddecipfkgeomhkgjdhhijih](https://chrome.google.com/webstore/detail/paragraph-detection/klfchfdilddecipfkgeomhkgjdhhijih)

## setup from source
1. Clone the repository 
2. Use `npm install` to install the required node modules (requires [node.js](https://nodejs.org/))
3. Use `bower install` to install the dependencies (if bower however has not been made available by npm, try installing it manually and globally with `npm -g install bower`)
4. Go to `chrome://extensions/`
5. Activate developer mode
6. Click on  `load an unpacked extension`, locate and select your cloned repo

## Logging
**important notice: it is necessary to start with clean tabs !!!**
The best solution to this is to adjust the settings to start with an empty tab when the browser starts and close/reopen the browser after each trial.

### Start/Stop
To start the logging, click the icon in the menu bar and hit `start log` in the Popup. To stop the logging, hit `save log`. Do not cancel the download of the logfile, otherwise the data will be lost. Starting/Stopping is not possible on Chrome-internal pages, such as chrome://extensions.

### Logfiles
The logfiles contain the following interactions (each with a timestamp and URL of the current page): 

* `initial` This entry is created when the logging starts. It contains the browser dimensions, and the positions/dimensions of the paragraphs extracted from currently active tab and their textual content and corresponding headline. 

* `newPage` Same as initial, but occurs, when a new page is visited.

* `paragraphFocused` This entry is created, when a paragraph is detected as focused or the focused paragraph changes. It contains the dimensions/positions of the paragraphs and the focused paragraph is indicated by a flag. 

* `scroll` When a scroll event occurs. Contains the current scroll position and dimension/positions of extracted paragraphs. (100ms timeout to counter subsequent scrolling) 

* `resize` When the window is resized/moved. Should not happen I guess, but who knows... same data as `initial`. (100ms timeout for resize and 500ms for move to counter for subsequent changes) 

* `click` X- and Y-coordinates of page and screen where the click happened. 

* `mouseMoved` X- and Y-coordinates of page and screen of the current mouse position. (200ms timeout to counter subsequent movements). 
